# Howto launch CadQuery to develop the 3D models

docker run -it --rm -v $PWD:/home/cq -p 8888:8888 bwalter42/jupyter_cadquery:3.5.2
